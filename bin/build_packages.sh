#!/usr/bin/env bash

set -exu

# This script is inteded to be called from the root directory of the repo

REPO_ROOT="$(pwd)"
PKGDEST="$REPO_ROOT/output"

mkdir "$PKGDEST" -p
chown builder:builder "$PKGDEST" -R
chown builder:builder "$REPO_ROOT" -R

for dir in pkgs/* ; do
  cd "$dir"
  sudo -u builder PKGDEST="$PKGDEST" makepkg --syncdeps --noconfirm --config "$REPO_ROOT/config/makepkg.conf"

  cd "$REPO_ROOT"
done

ls output
