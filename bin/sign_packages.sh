#!/usr/bin/env bash

set -exu

for pkg in output/* ; do

  echo "Signing $pkg"
  gpg --output "$pkg.sig" --detach-sig "$pkg"

  echo "Adding $pkg to repository"
  repo-add "output/joru.db.tar.xz" "$pkg" --sign --verify
done
